# phone-search-reverse
[![pipeline status](https://gitlab.com/smueller18/phone-search-reverse/badges/master/pipeline.svg)](https://gitlab.com/smueller18/phone-search-reverse/commits/master)
[![documentation](https://smueller18.gitlab.io/phone-search-reverse/documentation.svg)](https://smueller18.gitlab.io/phone-search-reverse/)
[![coverage](https://gitlab.com/smueller18/phone-search-reverse/badges/master/coverage.svg)](https://smueller18.gitlab.io/phone-search-reverse/coverage/)
[![pylint](https://smueller18.gitlab.io/phone-search-reverse/lint/pylint.svg)](https://smueller18.gitlab.io/phone-search-reverse/lint/)

## Set up

```bash
# Copy the environment file `template.env` to `.env`
cp template.env .env

# and set the variables.

# Create the `action.json` file
source .env
sed -e "s/<PROJECT_ID>/$PROJECT_ID/g" -e "s/<SERVER_NAME>/$SERVER_NAME/g" action.template.json > action.json

# Register the action
gactions update --project $PROJECT_ID --action_package action.json
```
