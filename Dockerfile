FROM python:3-alpine

COPY requirements.txt /app/requirements.txt
COPY app.py /app/app.py
COPY phone_search_reverse /app/phone_search_reverse

RUN apk add --no-cache --virtual .build-deps gcc musl-dev libffi-dev libressl-dev && \
    pip install --no-cache-dir -r /app/requirements.txt && \
    apk del .build-deps

EXPOSE 8000

CMD ["python", "/app/app.py"]