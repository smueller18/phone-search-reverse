import unittest

from phone_search_reverse import PhoneSearchReverseHandler

__author__ = "Stephan Müller"
__copyright__ = "2018, Stephan Müller"
__license__ = "MIT"


class PhoneSearchReverseTests(unittest.TestCase):

    @staticmethod
    def test_get_reverse_phone_info_from_html():
        html = """
            <div itemtype="http://schema.org/Person">
                <span itemprop="name">Mustermann Max</span>
                <div itemprop="address" itemtype="http://schema.org/PostalAddress">
                    <span itemprop="streetAddress">Hauptstra&szlig;e </span>1,
                    <span itemprop="postalCode">12345</span>
                    <span itemprop="addressLocality">Musterhausen</span>
                </div>
            </div>
        """

        response = PhoneSearchReverseHandler.get_reverse_phone_info_from_html(html)


if __name__ == '__main__':
    unittest.main()
