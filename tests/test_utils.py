import unittest

from phone_search_reverse import utils

__author__ = "Stephan Müller"
__copyright__ = "2018, Stephan Müller"
__license__ = "MIT"


class StructUtilsTests(unittest.TestCase):

    def test_trim(self):
        assertions = [
            ("Hello", "  Hello  "),
            ("HelloWorld", "  Hello\tWorld  "),
            ("Hello World", "  Hello\t\n World  "),
            ("Hello World", "  Hello\t\n World  \t"),
            ("Hello World", "  Hello\t\n World  \t  "),
            ("Hello World", "  Hello\t\n World  \t  \n"),
        ]
        for assertion in assertions:
            self.assertEqual(assertion[0], utils.trim(assertion[1]))


if __name__ == '__main__':
    unittest.main()
