"""Utils for simplification"""

import re

__author__ = u'Stephan Müller'
__copyright__ = u'2018, Stephan Müller'
__license__ = u'MIT'


def trim(text: str):
    """
    Remove all tabs, newline characters and surrounding space characters
    :param text: text to trim
    :return: trimmed text
    :rtype: str
    """
    return re.sub(r"[\n\t]*", "", text).strip()
