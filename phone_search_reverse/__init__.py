import logging
import json
from google.oauth2 import id_token
from google.auth.transport import requests as google_requests

import jwt
import requests
from bs4 import BeautifulSoup
import tornado.web

from phone_search_reverse import utils

__author__ = u'Stephan Müller'
__copyright__ = u'2018, Stephan Müller'
__license__ = u'MIT'

logger = logging.getLogger(__name__)


class PhoneSearchReverseHandler(tornado.web.RequestHandler):

    project_id = None
    phone_api_url = None

    def __init__(self, application, request):
        super().__init__(application, request)

        if self.project_id is None:
            raise AttributeError("project_id is not set")

        if self.phone_api_url is None:
            raise AttributeError("phone_api_url is not set")

    def data_received(self, chunk):
        super().data_received(chunk)

    def write_response(self, response):
        self.set_header("Content-Type", 'application/json')
        self.set_header('Google-Assistant-API-Version', 'v2')
        response_json = json.dumps(response, indent=2)
        self.write(response_json)
        logger.info(response_json)

    def start_conversation(self):
        self.write_response({
            'expectUserResponse': True,
            'expectedInputs': [{
                'possibleIntents': {'intent': 'actions.intent.TEXT'},
                'inputPrompt': {
                    'richInitialPrompt': {
                        'items': [{
                            'simpleResponse': {
                                'ssml': '<speak>'
                                        'Für welche Telefonnummer möchtest du die Rückwärtssuche ausführen?'
                                        '</speak>'
                            }
                        }]
                    }
                }
            }]
        })

    def get_reverse_phone_info(self, phone_number):
        response = self.get_reverse_phone_info_from_html(
                requests.get(self.phone_api_url + phone_number).content.decode())
        self.write_response(response)

    @staticmethod
    def get_reverse_phone_info_from_html(html):
        try:
            soup = BeautifulSoup(html, 'html5lib')
            person = soup.find(itemtype="http://schema.org/Person")
            postal_address = soup.find(itemtype="http://schema.org/PostalAddress")

            name = utils.trim(person.find(itemprop="name").text.replace("u.", "und"))
            street_address = utils.trim(postal_address.find(itemprop="streetAddress").text) + " " + \
                            utils.trim(postal_address.find(itemprop="streetAddress").nextSibling).strip(",")
            postal_code = utils.trim(postal_address.find(itemprop="postalCode").text)
            address_locality = utils.trim(postal_address.find(itemprop="addressLocality").text)

            return {
                'expectUserResponse': False,
                'finalResponse': {
                    'richResponse': {
                        'items': [{
                            'simpleResponse': {
                                'ssml': '<speak>Die Telefonnummer gehört {}, {}, '
                                        '<say-as interpret-as="verbatim">{}</say-as> {}'
                                        '</speak>'.format(name, street_address, postal_code, address_locality)
                            }
                        }]
                    }
                }
            }

        except:
            return {
                'expectUserResponse': False,
                'finalResponse': {
                    'richResponse': {
                        'items': [{
                            'simpleResponse': {
                                'ssml': '<speak>Es existiert kein Eintrag im Telefonbuch</speak>'
                            }
                        }]
                    }
                }
            }

    def get(self):
        phone_number = self.get_query_argument('phone_number', '')
        if phone_number:
            self.get_reverse_phone_info(phone_number)

        else:
            self.start_conversation()

    def post(self):
        token = self.request.headers.get("Authorization")
        jwt_data = jwt.decode(token, verify=False)
        if jwt_data['aud'] != self.project_id:
            self.set_status(401)
            self.write('Token Mismatch')

        else:
            request = google_requests.Request()
            try:
                # Makes external request, remove if not needed to speed things up
                id_info = id_token.verify_oauth2_token(token, request, self.project_id)
            except:
                self.set_status(401)
                self.write('Token Mismatch')

        data = json.loads(self.request.body.decode('utf-8'))
        intent = data['inputs'][0]['intent']
        logger.info(intent)
        logger.info(data['conversation']['conversationId'])

        if intent == 'actions.intent.MAIN':
            self.start_conversation()

        else:
            phone_number = data['inputs'][0]['arguments'][0]['textValue']
            self.get_reverse_phone_info(phone_number)

