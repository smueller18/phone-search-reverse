#!/usr/bin/python3

import os
import logging

import tornado.ioloop
import tornado.log
import tornado.web

from phone_search_reverse import PhoneSearchReverseHandler

__author__ = u'Stephan Müller'
__copyright__ = u'2018, Stephan Müller'
__license__ = u'MIT'


LOGGING_LEVEL = os.getenv("LOGGING_LEVEL", "INFO")
logging_format = "%(levelname)8s %(asctime)s %(message)s"
logging.basicConfig(level=logging.getLevelName(LOGGING_LEVEL), format=logging_format)

logger = logging.getLogger(__name__)

PROJECT_ID = os.getenv("PROJECT_ID", None)
PHONE_API_URL = os.getenv("PHONE_API_URL", None)


if __name__ == "__main__":

    logger.info("Starting tornado")
    tornado.log.enable_pretty_logging()

    PhoneSearchReverseHandler.project_id = PROJECT_ID
    PhoneSearchReverseHandler.phone_api_url = PHONE_API_URL
    app = tornado.web.Application([
        ("/" + PROJECT_ID, PhoneSearchReverseHandler),
    ], autoreload=True)
    app.listen(int(os.environ.get('PORT', '8000')))
    tornado.ioloop.IOLoop.current().start()
